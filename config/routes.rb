Counter::Application.routes.draw do
  root 'users#new'
  resources :users, path: '/', as: :users
  # resources :users

  # resources :users, :defaults => { :format => "json" }

  
  match '/users/login', to: 'users#login', via: :post
  match '/users/add',   to: 'users#add',   via: :post
  match '/TESTAPI/resetFixture', to: 'users#reset', via: :post
  match '/TESTAPI/unitTests',    to: 'users#tests', via: :post


end
